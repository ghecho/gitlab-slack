Licensed under BSD

http://opensource.org/licenses/BSD-2-Clause

Copyright (c) 2014, Diego Marquez Arzate

# Gitlab - Slack translation

This server translates the webhooks format of gitlab to the one in slack

It uses the node-slack library (https://github.com/xoxco/node-slack) to send the messages to slack.

At the moment only a very small and basic implementation exists.




It currently runs on OpenShift

The OpenShift `nodejs` cartridge documentation can be found at:

https://github.com/openshift/origin-server/tree/master/cartridges/openshift-origin-cartridge-nodejs/README.md


### Aditional info

You can check this page for more info on how to set up the integration https://github.com/gitlabhq/gitlabhq/blob/133da280582e5cdb003a2d2bc6885b21f861432c/doc/integration/slack.md

