#!/bin/env node
//  OpenShift sample Node application
var http = require('http');
var Slack = require('node-slack');
var slack = new Slack("your-domain-here","your-token-here"); // Domain is the first part of your <domain>.slack.com

//Get the environment variables we need.
var ipaddr  = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
var port    = process.env.OPENSHIFT_NODEJS_PORT || 8080;

http.createServer(function (request, response)
{
	if (request.method == 'POST')
	{
        var body = "";
        request.on('data', function (data)
        {
            body += data;
        });
        request.on('end', function ()
        {

        	var POST = JSON.parse(body);
            console.log(POST);

            if ('commits' in POST)
            {
            	if(POST.total_commits_count > 0)
            	{
            		slack.send
					({
						text: "New commit from:" + POST.commits[0].author + " \nmessage:" + POST.commits[0].message + "\nurl:" + POST.commits[0].url,
						channel: '#general',
						username: 'Gitlab'
					});
            	}
            }
            else if('object_kind' in POST)
            {
            	if(POST.object_kind == "issue")
            	{
            		slack.send
					({
						text: "Issue update:" + POST.object_attributes.title + "\nstatus:" + POST.object_attributes.state,
						channel: '#general',
						username: 'Gitlab'
					});
            	}
            }
            else
            {
            	response.writeHead(400, {"Content-Type": "text/plain"});
  				response.end("FAIL");
  				return;
            }

            response.writeHead(200, {"Content-Type": "text/plain"});
  			response.end("OK");
        });
    }
}).listen(port, ipaddr);





